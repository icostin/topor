extern crate clap;

pub fn bin_to_hex (input: &mut std::io::Read, output: &mut std::io::Write) -> std::io::Result<()> {
    let mut buffer = [0u8; 4096];
    loop {
        let n = input.read(&mut buffer)?;
        if n == 0 { break; }
        let data = &buffer[0..n];
        for b in data {
            write!(output, "{:02X}", b)?;
        }
    }
    writeln!(output, "")?;
    Ok(())
}

#[test]
fn test_bin_to_hex_simple () {
    let mut i = std::io::Cursor::new("AbcDo");
    let mut o = std::io::Cursor::new(Vec::<u8>::new());
    bin_to_hex(&mut i, &mut o).expect("should convert");
    assert_eq!(o.get_ref(), b"416263446F\n");
}

#[test]
fn test_bin_to_hex_overflow () {
    let mut i = std::io::Cursor::new("AbcD");
    let mut b = [0u8; 7];
    let mut o = std::io::Cursor::new(&mut b[..]);
    assert_eq!(bin_to_hex(&mut i, &mut o).expect_err("should fail").kind(), std::io::ErrorKind::WriteZero);
}


////////////////////////////////////////////////////////////////////////////////
pub fn hex_to_bin (input: &mut std::io::Read, output: &mut std::io::Write) -> std::io::Result<()> {
    let mut buffer = [0u8; 4096];
    let mut prev : Option<u8> = None;
    loop {
        let n = input.read(&mut buffer)?;
        if n == 0 { break; }
        let data = &buffer[0..n];
        for ch in data {
            let nibble = match ch {
                0x20 | (0x09 ... 0x0D) => {
                    if prev == None { continue }
                    return Err(std::io::Error::new(std::io::ErrorKind::Other, "whitespace after incomplete byte"))
                },
                (0x30 ... 0x39) => (ch - 0x30),
                (0x41 ... 0x46) => (ch - 0x41 + 10),
                (0x61 ... 0x66) => (ch - 0x61 + 10),
                _ => return Err(std::io::Error::new(std::io::ErrorKind::Other, "bad hex value"))
            };
            match prev {
                Some(high_nibble) => {
                    let b : u8 = (high_nibble << 4) | nibble;
                    output.write_all(std::slice::from_ref(&b))?;
                    prev = None;
                },
                None => {
                    prev = Some(nibble);
                }
            }
        }
    }
    Ok(())
}

#[test]
fn test_hex_to_bin_simple () {
    let mut i = std::io::Cursor::new("4D5a0A494F4f");
    let mut o = std::io::Cursor::new(Vec::<u8>::new());
    hex_to_bin(&mut i, &mut o).expect("should convert");
    assert_eq!(o.get_ref(), b"MZ\nIOO");
}

#[test]
fn test_hex_to_bin_deny_space_between_nibbles () {
    let mut i = std::io::Cursor::new("1 1");
    let mut o = std::io::Cursor::new(Vec::<u8>::new());
    assert_eq!(hex_to_bin(&mut i, &mut o).expect_err("should fail").kind(), std::io::ErrorKind::Other);
}

////////////////////////////////////////////////////////////////////////////////
fn cmd_hex (_am: &clap::ArgMatches) -> i32 {
    let stdin = std::io::stdin();
    let mut input = stdin.lock();
    let stdout = std::io::stdout();
    let stdout_lock = stdout.lock();
    let mut output = std::io::BufWriter::new(stdout_lock);

    match bin_to_hex(&mut input, &mut output) {
        Ok(()) => 0,
        _ => 1
    }
}

fn cmd_unhex (_am: &clap::ArgMatches) -> i32 {
    let stdin = std::io::stdin();
    let mut input = stdin.lock();
    let stdout = std::io::stdout();
    let stdout_lock = stdout.lock();
    let mut output = std::io::BufWriter::new(stdout_lock);

    match hex_to_bin(&mut input, &mut output) {
        Ok(()) => 0,
        _ => 1
    }
}

fn cmd_test (am: &clap::ArgMatches) -> i32 {
    let v = am.occurrences_of("verbosity");
    if v >= 4 {
        println!("test!");
    }

    return 0;
}


fn topor_main (a: &Vec<String>) -> i32 {
    let app = clap::App::new("topor")
        .version("0.00")
        .author("Costin Ionescu <costin.ionescu@gmail.com>")
        .about("chops and inspects data chunks")
        .arg(clap::Arg::with_name("verbosity")
            .short("v")
            .long("verbosity")
            .multiple(true)
            .help("sets the verbosity level")
            .takes_value(false))
        .subcommand(clap::SubCommand::with_name("test")
            .about("runs tests"))
        .subcommand(clap::SubCommand::with_name("hex")
            .about("converts binary data to hex"))
        .subcommand(clap::SubCommand::with_name("unhex")
            .about("converts hex data to binary"))
        ;
    let am = app.get_matches_from(a);

    let v = am.occurrences_of("verbosity");
    if v >= 4 {
        println!("am: {:?}", am);
    }

    let exit_code = match am.subcommand_name() {
        Some("test") => {
            cmd_test(&am)
        }
        Some("hex") => {
            cmd_hex(&am)
        }
        Some("unhex") => {
            cmd_unhex(&am)
        }
        _ => {
            println!("topor: run with '-h' or '--help' to see usage hints");
            1
        }
    };

    if v >= 4 {
        println!("cmd: {:?} -> exit_code={}", am.subcommand_name(), exit_code);
    }

    return exit_code;
}

fn main () {
    let args = std::env::args().collect();
    let exit_code = topor_main(& args);
    std::process::exit(exit_code);
}


