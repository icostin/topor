.PHONY: default
default: test-debug

PREFIX_DIR := $(HOME)/.local


.PHONY: test-debug
test-debug: build-debug
	cargo test
	./target/debug/topor -vvvv test

.PHONY: test-release
test-release: build-release
	cargo build --release
	./target/release/topor test

.PHONY: build-debug
build-debug:
	cargo build

.PHONY: build-release
build-release:
	cargo build --release

.PHONY: install
install: test-release
	install -t $(PREFIX_DIR)/bin ./target/release/topor

install-debug: test-debug
	install ./target/debug/topor $(PREFIX_DIR)/bin/topor-dbg
